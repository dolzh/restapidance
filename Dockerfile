FROM net5
RUN ./dotnet-install.sh -runtime aspnetcore --version 3.1.0
RUN dotnet tool install --global dotnet-ef
ENV PATH="$PATH:/root/.dotnet/tools"
ENV DOTNET_ROOT="/root/.dotnet"
WORKDIR /home/app
ENTRYPOINT ["dotnet", "watch", "run", "--urls", "https://0.0.0.0:5001;http://0.0.0.0:5000"]
#ENTRYPOINT ["sh"]